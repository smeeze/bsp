// system
#include <cassert>
#include <iostream>
// bsp
#include <smeeze/bsp/statistics.h>
#include <smeeze/bsp/tree.h>
// test
#include <test/number_split.h>
///////////////////////////////////////////////////////////////////////////////
int
main(int argc, char ** argv) {
  std::vector<int> numbers(32);
  smeeze::bsp::tree<smeeze::bsp::node<int, test::number_split<int>, int>> bsp(numbers.begin(), numbers.end());
  assert(smeeze::bsp::statistics::size(bsp.get_root()) == numbers.size());
  return 0;
}
