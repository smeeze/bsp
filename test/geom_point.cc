// test
#include <test/geom/point.h>
///////////////////////////////////////////////////////////////////////////////
int
main(int argc, char **argv) {
  test::geom::point p0(2, 2);
  test::geom::point p1(3, 4);

  std::cerr << "p0: " << p0 << std::endl;
  std::cerr << "p1: " << p1 << std::endl;
  std::cerr << "(p0 + p1): " << (p0 + p1) << std::endl;

  std::cerr << "(p0 + 1): " << (p0 + 1) << std::endl;
  std::cerr << "(p0 - 1): " << (p0 - 1) << std::endl;
  std::cerr << "(p0 * 2): " << (p0 * 2) << std::endl;
  std::cerr << "(p0 / 2): " << (p0 / 2) << std::endl;

  std::cerr << "p0.normalized(): " << p0.normalized() << std::endl;
  std::cerr << "p0.normalized().length(): " << p0.normalized().length() << std::endl;


  std::cerr << "p0.normalized().dot(p1.normalized()): " << p0.normalized().dot(p1.normalized()) << std::endl;
  return 0;
}
