// system
#include <algorithm>
#include <iostream>
#include <stack>
#include <vector>
// bsp
#include <smeeze/bsp/adapters/back_to_front.h>
#include <smeeze/bsp/adapters/front_to_back.h>
#include <smeeze/bsp/tree.h>
// test
#include <test/factory.h>
///////////////////////////////////////////////////////////////////////////////
template<typename Tree, typename Pivot>
void test_iterate(const Tree & t, const Pivot &p) {
  auto view(t.template view<smeeze::bsp::adapters::front_to_back<typename Tree::node_type>>(p));

  std::cerr << "tree:";
  for(const auto & v: view) {
    std::cerr << " " << v;
  }
  std::cerr << std::endl;
}
///////////////////////////////////////////////////////////////////////////////
int
main(int argc, char ** argv) {
  test_iterate(test::factory::build_123456(), 50);

  test_iterate(test::factory::build_random<int>(), 50);

  return 0;
}
