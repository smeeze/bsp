// system
#include <iostream>
#include <queue>
// test
#include <test/factory.h>
///////////////////////////////////////////////////////////////////////////////
template<typename node>
void dump_recursive_depth_first_pre_order(const node * n) {
  std::cerr << " " << n->value;
  if(n->get_front() != nullptr) { dump_recursive_depth_first_pre_order(n->get_front()); }
  if(n->get_back() != nullptr) { dump_recursive_depth_first_pre_order(n->get_back()); }
}
///////////////////////////////////////////////////////////////////////////////
template<typename node>
void dump_recursive_depth_first_in_order(const node * n) {
  if(n->get_front() != nullptr) { dump_recursive_depth_first_in_order(n->get_front()); }
  std::cerr << " " << n->value;
  if(n->get_back() != nullptr) { dump_recursive_depth_first_in_order(n->get_back()); }
}
///////////////////////////////////////////////////////////////////////////////
template<typename node>
void dump_recursive_depth_first_post_order(const node * n) {
  if(n->get_front() != nullptr) { dump_recursive_depth_first_post_order(n->get_front()); }
  if(n->get_back() != nullptr) { dump_recursive_depth_first_post_order(n->get_back()); }
  std::cerr << " " << n->value;
}
///////////////////////////////////////////////////////////////////////////////
template<typename node>
void dump_recursive_queue(std::queue<const node *> & q) {
  if(q.empty()) {
    return;
  }

  auto & n(q.front());
  q.pop();
  std::cerr << " " << n->value;

  if(n->get_front()) { q.push(n->get_front()); }
  if(n->get_back()) { q.push(n->get_back()); }

  dump_recursive_queue(q);
}
///////////////////////////////////////////////////////////////////////////////
template<typename node>
void dump_recursive_queue(const node * n) {
  std::queue<const node *> q;
  q.push(n);
  dump_recursive_queue(q);
}
///////////////////////////////////////////////////////////////////////////////
int
main(int argc, char ** argv) {
  auto tree(test::factory::build_123456());

  std::cerr << "depth first(pre order):";
  dump_recursive_depth_first_pre_order(tree.get_root());
  std::cerr << std::endl;

  std::cerr << "depth first(in order):";
  dump_recursive_depth_first_in_order(tree.get_root());
  std::cerr << std::endl;

  std::cerr << "depth first(post order):";
  dump_recursive_depth_first_post_order(tree.get_root());
  std::cerr << std::endl;

  std::cerr << "queue:";
  dump_recursive_queue(tree.get_root());
  std::cerr << std::endl;

  return 0;
}
