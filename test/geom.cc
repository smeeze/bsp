// test
#include <test/geom/line.h>
///////////////////////////////////////////////////////////////////////////////
int main(int argc, char ** argv) {
  test::geom::line line(test::geom::point(0.4, 0.1), test::geom::point(0.7, 0.5));
  std::cerr << line << std::endl;
  return 0;
}
