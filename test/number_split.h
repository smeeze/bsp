#pragma once

// system
#include <algorithm>
#include <vector>

namespace test {
template<typename Number>
class number_split {
public:
  typedef Number pivot_type;

  template<typename Iterator>
  void operator()(const Iterator & first, const Iterator & last, std::vector<Number> & item_set, std::vector<Number> & front_set, std::vector<Number> & back_set) {
    assert(first != last);
#if 0
    // find middle item
    auto pivot = *std::next(first, std::distance(first, last)/2);

    // partition
    Iterator middle = std::partition(first, last, [pivot](const Item & em){ return em < pivot; });

    // populate item set
    item_set.emplace_back(*middle);

    // populate front and back set
    front_set.assign(first, middle);
    back_set.assign(std::next(middle, 1), last);
#endif
    auto pivot(*first);
    for(auto i(first);i!=last;++i) {
      if(*i == pivot) { item_set.emplace_back(*i); }
      if(*i < pivot) { front_set.emplace_back(*i); }
      if(*i > pivot) { back_set.emplace_back(*i); }
    }
  }
};
}
