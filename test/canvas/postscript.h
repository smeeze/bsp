#pragma once

// system
#include <fstream>
// test
#include <test/canvas/interface.h>

namespace test {
namespace canvas {
class postscript : public test::canvas::interface {
public:
  postscript(const std::string & filename) : _file_stream(filename), _width(800), _height(600) {
    _file_stream << "%!PS" << std::endl;
    _file_stream << "%%DocumentMedia: TestPage " << _width << " " << _height << " 72 (test) (media)" << std::endl;
    _file_stream << "%%BeginDefaults" << std::endl;
    _file_stream << "%%PageMedia: TestPage" << std::endl;
    _file_stream << "%%EndDefaults" << std::endl;
    _file_stream << "%%BeginSetup" << std::endl;
    _file_stream << "%%PaperSizeTestPageA4" << std::endl;
    _file_stream << "%%EndSetup" << std::endl;
  // dot procedure
    _file_stream << "/dot {" << std::endl;
    _file_stream << "  newpath" << std::endl;
    _file_stream << "  1 0 360 arc" << std::endl;
    _file_stream << "  stroke" << std::endl;
    _file_stream << "} bind def" << std::endl;
  }
  ~postscript() {
    _file_stream << "showpage" << std::endl;
  }
  void draw_point(const test::geom::point & c) const override {
    _out(c);
    _file_stream << " dot" << std::endl;
  }
  void draw_line(const test::geom::point & p1, const test::geom::point & p2) const override {
    // _file_stream << "0.1 setlinewidth" << std::endl;
    _out(p1);
    _file_stream  << " newpath moveto" << std::endl;
    _out(p2);
    _file_stream  << " lineto" << std::endl;
    _file_stream << "stroke" << std::endl;
  }
  void draw_text(const test::geom::point & c, const std::string & msg) const override {
    _file_stream << "/DejaVuSans findfont" << std::endl;
    _file_stream << "24 scalefont" << std::endl;
    _file_stream << "setfont" << std::endl;
    _file_stream << "newpath" << std::endl;
    _out(c);
    _file_stream << " moveto" << std::endl;
    _file_stream << "(" << msg << ") show" << std::endl;
  }
  void set_color(float r, float g, float b) const override {
    _file_stream << r << " " << g << " " << b << " setrgbcolor" << std::endl;
  }
  void set_line_with(float f) const override {
    _file_stream << f << " setlinewidth" << std::endl;
  }
private:
  void _out(const test::geom::point & c) const {
    _file_stream << c.get_x() * _width << " " << c.get_y() * _height;
  }
private:
  mutable std::ofstream _file_stream;
  int                   _width;
  int                   _height;
};
}
}
