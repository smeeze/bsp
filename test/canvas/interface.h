#pragma once

// system
#include <string>
// test
#include <test/geom/line.h>
#include <test/geom/point.h>

namespace test {
namespace canvas {
class interface {
public:
  virtual ~interface() { } 

  virtual void draw_point(const test::geom::point &) const = 0;
  virtual void draw_line(const test::geom::line &l) const { draw_line(l.get_p1(), l.get_p2()); }
  virtual void draw_line(const test::geom::point &, const test::geom::point &) const = 0;
  virtual void draw_text(const test::geom::point &, const std::string &) const = 0;

  virtual void draw_square(const test::geom::point & p1, const test::geom::point & p2) const {
    draw_line(test::geom::point(p1.get_x(), p1.get_y()), test::geom::point(p2.get_x(), p1.get_y()));
    draw_line(test::geom::point(p2.get_x(), p1.get_y()), test::geom::point(p2.get_x(), p2.get_y()));
    draw_line(test::geom::point(p2.get_x(), p2.get_y()), test::geom::point(p1.get_x(), p2.get_y()));
    draw_line(test::geom::point(p1.get_x(), p2.get_y()), test::geom::point(p1.get_x(), p1.get_y()));
  }

  virtual void set_color(float r, float g, float b) const { }
  virtual void set_line_with(float f) const { }
};
}
}
