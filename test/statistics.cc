// system
#include <iostream>
// bsp
#include <smeeze/bsp/statistics.h>
#include <smeeze/bsp/tree.h>
// test
#include <test/factory.h>
#include <test/number_split.h>
///////////////////////////////////////////////////////////////////////////////
int
main(int argc, char ** argv) {
  auto bsp(test::factory::build_random<int>());
  std::cerr << "count: " << smeeze::bsp::statistics::count(bsp.get_root()) << std::endl;
  std::cerr << "size: " << smeeze::bsp::statistics::size(bsp.get_root()) << std::endl;
  std::cerr << "depth: " << smeeze::bsp::statistics::depth(bsp.get_root()) << std::endl;

  return 0;
}
