// system
// boost
#include <boost/format.hpp>
// bsp
#include <smeeze/bsp/adapters/front_to_back.h>
#include <smeeze/bsp/node.h>
#include <smeeze/bsp/tree.h>
// test
#include <test/algorithm/geom.h>
#include <test/canvas/postscript.h>
#include <test/geom/line.h>
#include <test/geom/point.h>
#include <test/factory.h>
///////////////////////////////////////////////////////////////////////////////
const size_t point_count = 16 * 1024;
///////////////////////////////////////////////////////////////////////////////
template<typename Node>
class my_adapter {
private:
  typedef          const Node          * node_pointer;
public:
  typedef          Node                  node_type;
  typedef typename Node::value_type      value_type;
  typedef typename Node::iterator_type   iterator_type;
  typedef typename Node::pivot_type      pivot_type;

  my_adapter(node_pointer n) : _node(n), _first(nullptr), _second(nullptr) {
  }

  void pivot(const pivot_type & p) {
    auto c(_node->get_partition().get_line().classify(p));
    if(c < 0) {
      _first = _node->get_back();
      _second = _node->get_front();
    } else {
      _first = _node->get_front();
      _second = _node->get_back();
    }
  }

  iterator_type begin() const { return _node->begin(); }
  iterator_type end() const { return _node->end(); }

  node_pointer get_node() const { return _node; }
  node_pointer get_first() const { return _first; }
  node_pointer get_second() const { return _second; }
private:
  node_pointer _node;
  node_pointer _first;
  node_pointer _second;
};
///////////////////////////////////////////////////////////////////////////////
class my_partition {
public:
  my_partition() : _line({0, 0}, {0, 0}) { }

  template<typename Iterator, typename Vector>
  void operator()(const Iterator & begin, const Iterator & end, Vector & items, Vector & front_set, Vector & back_set) {
    auto fp(test::algorithm::geom::find_farthest_points(begin, end));
    _line = test::geom::line(test::factory::build_random_point(), test::factory::build_random_point());

    for(auto i(begin); i!=end; ++i) {
      static const float threshold(0.1);
      auto f(_line.distance((*i)));
      if(f >  threshold) { front_set.emplace_back(*i); continue; }
      if(f < -threshold) { back_set.emplace_back(*i); continue; }
      items.emplace_back(*i);
    }
  }


  const test::geom::line & get_line() const {
    return _line;
  }
private:
  test::geom::line _line;
};
///////////////////////////////////////////////////////////////////////////////
template<typename Node>
void
render_tree_parititions(const std::unique_ptr<test::canvas::interface> & canvas, const Node * node, float depth = 1) {
  if(node->get_front() != nullptr) { render_tree_parititions(canvas, node->get_front(), depth * 0.5); }
  if(node->get_back() != nullptr) { render_tree_parititions(canvas, node->get_back(), depth * 0.5); }

  canvas->set_color(0.8, 0.5, 0.5);
  canvas->draw_line(node->get_partition().get_line());
}
///////////////////////////////////////////////////////////////////////////////
template<typename Tree, typename Pivot>
void
render_tree_items(const std::unique_ptr<test::canvas::interface> & canvas, const Tree & tree, const Pivot &p) {
  uint32_t n(0);
  auto view(tree.template view<my_adapter<typename Tree::node_type>>(p));
  for(const auto & p: view) {
    float f( (float)(++n) / point_count);
    canvas->set_color(f, f, f);
    canvas->draw_point(p);
  }
}
///////////////////////////////////////////////////////////////////////////////
int
main(int argc, char ** argv) {
  std::vector<test::geom::point> point_cloud(point_count);
  for(auto & p: point_cloud) {
      p=test::factory::build_random_point();
  }
  smeeze::bsp::tree<smeeze::bsp::node<test::geom::point, my_partition, test::geom::point>> tree(point_cloud.begin(), point_cloud.end());

  std::unique_ptr<test::canvas::interface> canvas(new test::canvas::postscript("test_tree_render1.ps"));

  // render_tree_parititions(canvas, tree.get_root());
  render_tree_items(canvas, tree, test::geom::point(0, 0));

  return 0;
}
