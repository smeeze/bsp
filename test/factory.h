#pragma once

// system
#include <random>
// bsp
#include <smeeze/bsp/node.h>
#include <smeeze/bsp/tree.h>
#include <smeeze/bsp/view.h>
// test
#include <test/number_split.h>
#include <test/geom/point.h>
///////////////////////////////////////////////////////////////////////////////
namespace test {
class factory {
public:
  template<typename Number>
  static
  smeeze::bsp::tree<smeeze::bsp::node<Number, test::number_split<Number>, Number>>
  build_random(size_t n = 32) {
    auto my_rand([]()->Number{return std::rand() % 128;});
    std::vector<Number> numbers(n);
    std::generate(numbers.begin(), numbers.end(), my_rand);

    // build tree
    return smeeze::bsp::tree<smeeze::bsp::node<Number, test::number_split<Number>, Number>>(numbers.begin(), numbers.end());
  }
public:
#if 1
  struct simple_node {
    typedef simple_node        node_type;
    typedef int                value_type;
    typedef const value_type * iterator_type;
    typedef int                pivot_type;

    simple_node(const value_type & v) : value(v), front(nullptr), back(nullptr) { }

    const simple_node * get_front() const { return front.get(); }
    const simple_node * get_back() const { return back.get(); }

    iterator_type begin() const { return & value; }
    iterator_type end() const { return (& value) + 1; }

    value_type                   value;
    std::shared_ptr<simple_node> front;
    std::shared_ptr<simple_node> back;
  };
public:
  static
  smeeze::bsp::tree<simple_node>
  build_123456() {
    simple_node root(0);

    root.front.reset(new simple_node(1));
    root.back.reset(new simple_node(2));
    root.front->front.reset(new simple_node(3));
    root.front->back.reset(new simple_node(4));
    root.back->front.reset(new simple_node(5));
    root.back->back.reset(new simple_node(6));

    return smeeze::bsp::tree<simple_node>(root);
  }
  static
  test::geom::point
  build_random_point() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 1);
    return test::geom::point(dis(gen), dis(gen));
  }
#endif
};
}
