#pragma once

// system
#include <cmath>
#include <iostream>

namespace test {
namespace geom {
class point {
public:
  point(const float & x = 0, const float & y = 0) : _x(x), _y(y) {
  }

  const float & get_x() const { return _x; }
  const float & get_y() const { return _y; }

  point & operator+=(const point &p)       { _x += p._x; _y += p._y; return *this; }
  point   operator+ (const point &p) const { return point(*this) += p; }
  point & operator-=(const point &p)       { _x -= p._x; _y -= p._y; return *this; }
  point   operator- (const point &p) const { return point(*this) -= p; }

  point & operator+=(const float &f)       { _x += f; _y += f; return *this; }
  point   operator+ (const float &f) const { return point(*this) += f; }
  point & operator-=(const float &f)       { _x -= f; _y -= f; return *this; }
  point   operator- (const float &f) const { return point(*this) -= f; }
  point & operator*=(const float &f)       { _x *= f; _y *= f; return *this; }
  point   operator* (const float &f) const { return point(*this) *= f; }
  point & operator/=(const float &f)       { _x /= f; _y /= f; return *this; }
  point   operator/ (const float &f) const { return point(*this) /= f; }

  float length()            const { return sqrt(_x * _x + _y * _y); }

  float dot(const point &p) const { return _x * p._x + _y * p._y; }

  point normalized() const { return (*this) / length(); }
private:
  float _x;
  float _y;
};
}
}

inline
std::ostream & operator<<(std::ostream & out, const test::geom::point & p) {
  out << "point(" << p.get_x() << ", " << p.get_y() << ")";
  return out;
}
