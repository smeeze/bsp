#pragma once

// system
#include <iostream>
// test
#include <test/geom/point.h>

namespace test {
namespace geom {
class line {
public:
  line(const test::geom::point & p1, const test::geom::point & p2) : _p1(p1), _p2(p2) {
  }

  const test::geom::point & get_p1() const { return _p1; }
  const test::geom::point & get_p2() const { return _p2; }

  point direction() const {
    return _p2 - _p1;
  }
  point perpendicular() const {
    return point(  get_p2().get_y() - get_p1().get_y(),
                 -(get_p2().get_x() - get_p1().get_x()));
  }
  point normal() const {
    return perpendicular().normalized();
  }
  float classify(const test::geom::point &p) const {
    return normal().dot( (p - _p1).normalized() );
  }
  float distance(const test::geom::point &p) const {
    return normal().dot(p - get_p1());
  }
private:
  test::geom::point _p1;
  test::geom::point _p2;
};
}
}

inline
std::ostream & operator<<(std::ostream & out, const test::geom::line & l) {
  out << "line(" << l.get_p1() << ", " << l.get_p2() << ")";
  return out;
}
