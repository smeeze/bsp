// system
#include <memory>
// test
#include <test/canvas/postscript.h>
#include <test/geom/line.h>
#include <test/geom/point.h>
#include <test/factory.h>
///////////////////////////////////////////////////////////////////////////////

int
main(int argc, char ** argv) {
  std::unique_ptr<test::canvas::interface> canvas(new test::canvas::postscript("test_geom_line.ps"));

  test::geom::line line(test::factory::build_random_point(), test::factory::build_random_point());

  for(int i=0;i<1000;++i) {
    auto point(test::factory::build_random_point());
    auto c(line.distance((point)));
    if(c > 0) {
      canvas->set_color(1, .5, .5);
    } else {
      canvas->set_color(.5, 1, .5);
    }
    canvas->draw_point(point);
  }

  canvas->set_color(0, 0, 0);
  canvas->set_line_with(2);
  canvas->draw_line(line);

  canvas->set_color(0.5, 0.5, 0.5);
  canvas->set_line_with(1);
  canvas->draw_line(line.get_p1(), line.get_p1() + line.normal() * 0.1);

  return 0;
}
