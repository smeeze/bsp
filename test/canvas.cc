// test
#include <test/canvas/postscript.h>
///////////////////////////////////////////////////////////////////////////////
void
render(const test::canvas::interface & canvas) {
  canvas.draw_square(test::geom::point(0.05, 0.05),
                     test::geom::point(0.95, 0.95));

  canvas.draw_point(test::geom::point(0.2, 0.2));

  canvas.draw_text(test::geom::point(0.2, 0.4), "test canvas");
}
///////////////////////////////////////////////////////////////////////////////
int
main(int argc, char ** argv) {
  render(test::canvas::postscript("test_canvas.ps"));

  return 0;
}
