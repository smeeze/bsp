// self
#include <test/algorithm/geom.h>
#include <test/canvas/postscript.h>
#include <test/factory.h>
///////////////////////////////////////////////////////////////////////////////
int
main(int argc, char ** argv) {
  std::vector<test::geom::point> point_cloud(10);
  for(auto & p: point_cloud) {
      p=test::factory::build_random_point();
  }

  std::unique_ptr<test::canvas::interface> canvas(new test::canvas::postscript("test_farthest_points.ps"));

  canvas->set_line_with(1);
  canvas->set_color(0, 0, 0);
  for(const auto & p: point_cloud) {
    canvas->draw_point(p);
  }

  auto fp(test::algorithm::geom::find_farthest_points(point_cloud.begin(), point_cloud.end()));
  canvas->set_line_with(5);
  canvas->set_color(1, 0, 0);
  canvas->draw_point(*fp.first);
  canvas->draw_point(*fp.second);
  return 0;
}
