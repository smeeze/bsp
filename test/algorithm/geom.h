// self
#include <test/geom/point.h>

namespace test {
namespace algorithm {
class geom {
public:
  template<typename Iterator>
  static
  std::pair<Iterator, Iterator>
  find_farthest_points(const Iterator & begin, const Iterator & end) {
    std::pair<Iterator, Iterator> rv(begin, begin);
    for(int pass=0;pass<2;++pass) {
      for(auto i(begin);i!=end;++i) {
        auto d( ( (*rv.first) - (*rv.second) ).length() );
        {
          auto tmp( ( (*i) - (*rv.second) ).length() );
          if(tmp > d) { rv.first = i; }
        }
        {
          auto tmp( ( (*i) - (*rv.first) ).length() );
          if(tmp > d) { rv.second = i; }
        }
      }
    }
    return rv;
  }
};
}
}
