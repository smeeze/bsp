// system
#include <iostream>
// bsp
#include <smeeze/bsp/render.h>
#include <smeeze/bsp/tree.h>
// test
#include <test/factory.h>
#include <test/number_split.h>
///////////////////////////////////////////////////////////////////////////////
int
main(int argc, char ** argv) {
  smeeze::bsp::render::left_to_right<>(test::factory::build_123456().get_root());
  smeeze::bsp::render::left_to_right<>(test::factory::build_random<int>().get_root());
  return 0;
}
